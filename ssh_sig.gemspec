# frozen_string_literal: true

require_relative "lib/ssh_sig/version"

Gem::Specification.new do |spec|
  spec.name          = "ssh_sig"
  spec.version       = SshSig::VERSION
  spec.authors       = ["Brian Williams"]
  spec.email         = ["bwilliams@gitlab.com"]

  spec.summary       = "SSH Signatures in Ruby"
  spec.description   = File.read('README.md')
  spec.homepage      = "https://gitlab.com/bwill/ssh_sig"
  spec.license       = "MIT"
  spec.required_ruby_version = ">= 2.7.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/bwill/ssh_sig"
  spec.metadata["changelog_uri"] = "https://gitlab.com/bwill/ssh_sig/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency 'bcrypt_pbkdf', '~> 1.1'
  spec.add_runtime_dependency 'ed25519', '~> 1.2'
  spec.add_runtime_dependency 'net-ssh', '>= 6.3.0.beta1'
  spec.add_runtime_dependency 'zeitwerk', '~> 2.4'

  spec.add_development_dependency 'gitlab-styles', '~> 6.2.0'
  spec.add_development_dependency 'rspec-parameterized', '~> 0.5.0'
end
