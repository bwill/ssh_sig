# frozen_string_literal: true
module SshSig
  module Serializable
    SIG_VERSION = 1
    MAGIC_PREAMBLE = "SSHSIG"
    BEGIN_SIGNATURE = "-----BEGIN SSH SIGNATURE-----"
    END_SIGNATURE = "-----END SSH SIGNATURE-----"
    SIGALG_ALLOWED = %w[ssh-ed25519 rsa-sha2-512 rsa-sha2-256].freeze
    HASHALG_ALLOWED = %w[sha512 sha256].freeze

    def signature_algorithm_allowed?(alg)
      SIGALG_ALLOWED.any?(alg)
    end

    def hash_algorithm_allowed?(alg)
      HASHALG_ALLOWED.any?(alg)
    end
  end
end
