# frozen_string_literal: true
require 'net/ssh'

module SshSig
  module KeyLoader
    class PubKey
      SUPPORTED_KEY_ALGORITHMS = %w[ssh-ed25519 ssh-rsa].freeze
      class << self
        def load(armored)
          keys = armored.split("\n")

          keys
            .filter { |key| supported_key_algorithm?(key) }
            .map { |key| load_data_public_key(key) }
        rescue ::Net::SSH::Exception, ::ArgumentError
          raise ::SshSig::LoadError, 'Public key is not valid'
        end

        private

        def load_data_public_key(key)
          ::Net::SSH::KeyFactory.load_data_public_key(key)
        end

        def supported_key_algorithm?(key)
          alg = key.split(' ').first

          SUPPORTED_KEY_ALGORITHMS.any?(alg)
        end
      end
    end
  end
end
