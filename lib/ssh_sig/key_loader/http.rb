# frozen_string_literal: true

require 'open-uri'

module SshSig
  module KeyLoader
    class Http < PubKey
      class << self
        def load(url)
          keys = get(url)

          super(keys)
        end

        def load_dot_keys(username, base_addr = 'https://gitlab.com')
          load("#{base_addr}/#{username}.keys")
        end

        private

        def get(url)
          URI(url).read
        rescue StandardError => e
          raise ::SshSig::LoadError, "Error received from remote: #{e.message}"
        end
      end
    end
  end
end
