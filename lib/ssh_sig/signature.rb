# frozen_string_literal: true
module SshSig
  class Signature
    attr_reader :algorithm, :bytes

    def initialize(
      algorithm:,
      bytes:
    )
      @algorithm = algorithm
      @bytes = bytes
    end

    # While not described well in the protocol documenation,
    # the signature has two parts. There is a string describing the algorithm,
    # which can be one of ssh-ed25519, rsa-sha2-512, or rsa-sha2-256,
    # followed by the actual signature bytes.
    #
    # string   algorithm
    # string   signature_bytes
    def self.from_bytes(blob)
      buf = ::Net::SSH::Buffer.new(blob)

      algorithm = buf.read_string

      raise DecodeError, 'Signature algorithm is missing' if algorithm.nil?

      bytes = buf.read_string

      raise DecodeError, 'Signature data is missing' if bytes.nil?

      Signature.new(
        algorithm: algorithm,
        bytes: bytes
      )
    end
  end
end
