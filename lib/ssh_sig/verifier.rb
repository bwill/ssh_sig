# frozen_string_literal: true

require 'ed25519'

module SshSig
  class Verifier
    def initialize(public_keys)
      @public_keys = public_keys
    end

    def self.from_armored_pubkey(armored_pubkey)
      public_keys = ::SshSig::KeyLoader::PubKey.load(armored_pubkey)

      new(public_keys)
    end

    def self.from_github(username, base_addr = 'https://github.com')
      public_keys = ::SshSig::KeyLoader::Http.load_dot_keys(username, base_addr)

      new(public_keys)
    end

    def self.from_gitlab(username, base_addr = 'https://gitlab.com')
      public_keys = ::SshSig::KeyLoader::Http.load_dot_keys(username, base_addr)

      new(public_keys)
    end

    def verify(blob, message)
      return false unless blob&.signature

      @public_keys.any? do |key|
        key.ssh_do_verify(
          blob.signature.bytes,
          blob.signature_data(message),
          # When using RSA, net-ssh uses this to determine the digest algorithm to use.
          # Added in net-ssh 6.3.0.beta1
          { host_key: blob.signature.algorithm }
        )
      end
    rescue ::Ed25519::VerifyError
      # Ed25519 public keys raise exceptions when they fail to verify,
      # but RSA public keys don't
      false
    end
  end
end
