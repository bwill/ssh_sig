# frozen_string_literal: true
module SshSig
  Error = Class.new(::StandardError)
  DecodeError = Class.new(Error)
  LoadError = Class.new(Error)
  VerifyError = Class.new(Error)
end
