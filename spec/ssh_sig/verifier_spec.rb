# frozen_string_literal: true
require 'net/ssh/key_factory'

RSpec.describe SshSig::Verifier do
  # ssh-keygen -t ed25519 -f ed25519
  let(:armored_pubkey) { "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOja1kOLc2eq3XLNUc1ezjm+u07nNfgt2mTWN3H1kCzO" }
  let(:message) { "This is a test of SshSig::Verifier" }

  # echo -n "$message" > test.txt
  # ssh-keygen -Y sign -n file -f ed25519 test.txt
  let(:signature_file) do
    <<~EOF
    -----BEGIN SSH SIGNATURE-----
    U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAg6NrWQ4tzZ6rdcs1RzV7OOb67Tu
    c1+C3aZNY3cfWQLM4AAAAEZmlsZQAAAAAAAAAGc2hhNTEyAAAAUwAAAAtzc2gtZWQyNTUx
    OQAAAECpsNFHFUCz8V8BmjynWztkMeJciIk1gHc1/S2Fq2Fl/2lAfTa6hFa6n+e9PqYmLM
    UBi9/i4MdXG2YV0+Mbl40K
    -----END SSH SIGNATURE-----
    EOF
  end

  let(:blob) { ::SshSig::Blob.from_armor(signature_file) }

  subject(:verifier) { described_class.from_armored_pubkey(armored_pubkey) }

  describe '#from_armored_pubkey' do
    it 'returns SshSig::Verifier' do
      expect(described_class.from_armored_pubkey(armored_pubkey)).to be_a(described_class)
    end
  end

  describe '#verify' do
    context 'with Ed25519 public key' do
      context 'with valid signature' do
        it 'returns true' do
          expect(verifier.verify(blob, message)).to be true
        end
      end

      context 'with invalid signature' do
        let(:signature_file) do
          <<~EOF
          -----BEGIN SSH SIGNATURE-----
          U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAg6NrWQ4tzZ6rdcs1RzV7OOb67Tu
          c1+C3aZNY3cfWQLM4AAAAEZmlsZQAAAAAAAAAGc2hhNTEyAAAAUwAAAAtzc2gtZWQyNTUx
          OQAAAECpsNFHFUCz8V8BmjynWztkMeJciIk1gHc1/S2Fq2Fl/2lAfTa6hFa6n+e9PqYmLM
          UBi9/i4MdXG2YV0+Ot+t8N
          -----END SSH SIGNATURE-----
          EOF
        end

        it 'does not raise errors' do
          expect { verifier.verify(blob, message) }.not_to raise_error
        end

        it 'returns false' do
          expect(verifier.verify(blob, message)).to be false
        end
      end
    end

    context 'with RSA public key' do
      let(:armored_pubkey) do
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDDOoV2xhOqYQ6yJLYDJogmpJUBUW4+nQ+" \
        "yzkY8oc1GGyc8+QMKgakaKMya3YdmbYssEb6HBegeLznGhijld57DUMHr55XSBX6DLEGEA8" \
        "JmVlMXkY0cmB32QSIkg6QNF86tpl9N6Le+KupABUamzCAzdflIKl5E5rk/VFO+LJNsot1dW" \
        "f+iVKBmfjy4QvkWanO7nvnYEHJxLX5YtK9mH8SfDiP/V1kYu8qvKKXOMZB3OQYQt301HmPf" \
        "Xc8zqqzoLCLKBZpo7GjWVN6L6QcQmzwUXP7oNri3hh8pA4YApNF20rqjkOPGDtmX5NM0QkX" \
        "mTpjzrlUqfeYF0VxAq33TjVMb4jEKLkTtpQQbjP4A+pVMtFPlab9yghKQw7yqJ8BnXGJrfi" \
        "3n1M8mpQb6ieZ6fNxwN1fzx+dvfLCbz4BB9X7HItCkIhUswlz5L5+J9e3oGs7KGGreTu6U6" \
        "mv0a1uKskilsbhJx7Mc9jY326W5ya508nvn/RdxhQJuwyLDx6u6NbU="
      end

      context 'with valid signature' do
        let(:signature_file) do
          <<~EOF
          -----BEGIN SSH SIGNATURE-----
          U1NIU0lHAAAAAQAAAZcAAAAHc3NoLXJzYQAAAAMBAAEAAAGBAMM6hXbGE6phDrIktgMmiC
          aklQFRbj6dD7LORjyhzUYbJzz5AwqBqRoozJrdh2ZtiywRvocF6B4vOcaGKOV3nsNQwevn
          ldIFfoMsQYQDwmZWUxeRjRyYHfZBIiSDpA0Xzq2mX03ot74q6kAFRqbMIDN1+UgqXkTmuT
          9UU74sk2yi3V1Z/6JUoGZ+PLhC+RZqc7ue+dgQcnEtfli0r2YfxJ8OI/9XWRi7yq8opc4x
          kHc5BhC3fTUeY99dzzOqrOgsIsoFmmjsaNZU3ovpBxCbPBRc/ug2uLeGHykDhgCk0XbSuq
          OQ48YO2Zfk0zRCReZOmPOuVSp95gXRXECrfdONUxviMQouRO2lBBuM/gD6lUy0U+Vpv3KC
          EpDDvKonwGdcYmt+LefUzyalBvqJ5np83HA3V/PH5298sJvPgEH1fsci0KQiFSzCXPkvn4
          n17egazsoYat5O7pTqa/RrW4qySKWxuEnHsxz2NjfbpbnJrnTye+f9F3GFAm7DIsPHq7o1
          tQAAAARmaWxlAAAAAAAAAAZzaGE1MTIAAAGUAAAADHJzYS1zaGEyLTUxMgAAAYB0fUzzxA
          xTWJcCRsFnsbdV93oZC+xvykSKwOJkPStBfX7eMDX5fdVQ3rUJW6Us89F4OCRxoXCFlLtq
          pfwub5BBZPl0KldbBkUB+CzNL4K1ErZTm7eE2vUpJ2Ag1YjgFuTCFcimVuFLy1QA+7822j
          uuoh+4eF2gJ5DK4FsMbXNqbrIIzN3VSmQ+uFUsakR0j74hT7ExfhjoR/86S5oap+iIL1no
          Xmct5oWhkjmBtcf8YmE7RH8l72P7PfgAMaQ44A9nxrBNOrzVA3u5GcJoSt2sIdR9dntIJk
          uFCHCBGadb9LuCF2s3hndQ8uaQ9ZoA72AC/BeHfzr1xgq64T4cH9V98rvoZuUkrlMFB34K
          8inSnwP5spsBv1jAouqlhoXNWSTmBmWwFFREtrNRu0Q8MwywVdqnOAKHK0NZVi/LsOEGHX
          WNLG4rhfRmymI2YaoeWzWa+C/6Z8PoDGMNo54YoAYgOxE/3x1jNCsscgrdUXf6gEvsHxaX
          qjtz4zgWqbc4Nt8=
          -----END SSH SIGNATURE-----
          EOF
        end

        it 'does not raise errors' do
          expect { verifier.verify(blob, message) }.not_to raise_error
        end

        it 'returns true' do
          expect(verifier.verify(blob, message)).to be true
        end
      end

      context 'with invalid signature' do
        let(:signature_file) do
          <<~EOF
          -----BEGIN SSH SIGNATURE-----
          U1NIU0lHAAAAAQAAAZcAAAAHc3NoLXJzYQAAAAMBAAEAAAGBAMM6hXbGE6phDrIktgMmiC
          aklQFRbj6dD7LORjyhzUYbJzz5AwqBqRoozJrdh2ZtiywRvocF6B4vOcaGKOV3nsNQwevnl
          dIFfoMsQYQDwmZWUxeRjRyYHfZBIiSDpA0Xzq2mX03ot74q6kAFRqbMIDN1+UgqXkTmuT9U
          U74sk2yi3V1Z/6JUoGZ+PLhC+RZqc7ue+dgQcnEtfli0r2YfxJ8OI/9XWRi7yq8opc4xkHc
          5BhC3fTUeY99dzzOqrOgsIsoFmmjsaNZU3ovpBxCbPBRc/ug2uLeGHykDhgCk0XbSuqOQ48
          YO2Zfk0zRCReZOmPOuVSp95gXRXECrfdONUxviMQouRO2lBBuM/gD6lUy0U+Vpv3KCEpDDv
          KonwGdcYmt+LefUzyalBvqJ5np83HA3V/PH5298sJvPgEH1fsci0KQiFSzCXPkvn4n17ega
          zsoYat5O7pTqa/RrW4qySKWxuEnHsxz2NjfbpbnJrnTye+f9F3GFAm7DIsPHq7o1tQAAAAR
          maWxlAAAAAAAAAAZzaGE1MTIAAAGUAAAADHJzYS1zaGEyLTUxMgAAAYB0fUzzxAxTWJcCRs
          FnsbdV93oZC+xvykSKwOJkPStBfX7eMDX5fdVQ3rUJW6Us89F4OCRxoXCFlLtqpfwub5BBZ
          Pl0KldbBkUB+CzNL4K1ErZTm7eE2vUpJ2Ag1YjgFuTCFcimVuFLy1QA+7822juuoh+4eF2g
          J5DK4FsMbXNqbrIIzN3VSmQ+uFUsakR0j74hT7ExfhjoR/86S5oap+iIL1noXmct5oWhkjm
          Btcf8YmE7RH8l72P7PfgAMaQ44A9nxrBNOrzVA3u5GcJoSt2sIdR9dntIJkuFCHCBGadb9L
          uCF2s3hndQ8uaQ9ZoA72AC/BeHfzr1xgq64T4cH9V98rvoZuUkrlMFB34K8inSnwP5spsBv
          1jAouqlhoXNWSTmBmWwFFREtrNRu0Q8MwywVdqnOAKHK0NZVi/LsOEGHXWNLG4rhfRmymI2
          YaoeWzWa+C/6Zw2jnhigBiA7ET/fHWM0KyxyCt1Rd/qAS+wfFpeqO3PjOBaptzg23w==
          -----END SSH SIGNATURE-----
          EOF
        end

        it 'does not raise errors' do
          expect { verifier.verify(blob, message) }.not_to raise_error
        end

        it 'returns false' do
          expect(verifier.verify(blob, message)).to be false
        end
      end
    end
  end
end
