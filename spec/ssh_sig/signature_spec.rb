# frozen_string_literal: true
RSpec.describe SshSig::Signature do
  describe '#from_bytes' do
    subject(:from_bytes) { described_class.from_bytes(bytes) }

    context 'with ssh-ed25519 signature bytes' do
      let(:bytes) do
        hex_to_bin(
          <<~EOF
          00 00 00 0b 73 73 68 2d 65 64 32 35 35 31 39 00
          00 00 40 8c d3 ec ad 6e e8 cc 1d 59 9a ee b9 b2
          da 61 76 5b 7a 5b 9f 57 76 a1 39 82 23 3a 2e 18
          e3 93 b6 a3 5a 31 40 7c a0 00 25 91 b3 7d 30 66
          bc e7 07 73 01 b1 46 fd 2c 1f 8d 9c f3 65 67 df
          55 f7 07
          EOF
        )
      end

      it 'parses algorithm and data' do
        signature = from_bytes

        expect(signature.algorithm).to eq("ssh-ed25519")
        expect(signature.bytes).to eq_hex(
          <<~EOF
          8c d3 ec ad 6e e8 cc 1d 59 9a ee b9 b2 da 61 76
          5b 7a 5b 9f 57 76 a1 39 82 23 3a 2e 18 e3 93 b6
          a3 5a 31 40 7c a0 00 25 91 b3 7d 30 66 bc e7 07
          73 01 b1 46 fd 2c 1f 8d 9c f3 65 67 df 55 f7 07
          EOF
        )
      end
    end
  end
end
