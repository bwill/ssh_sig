# frozen_string_literal: true
RSpec.describe SshSig::Serializable do
  include described_class

  describe '#signature_algorithm_allowed?' do
    using RSpec::Parameterized::TableSyntax

    where(:algorithm, :expected) do
      "rsa-sha2-512"                                | true
      "rsa-sha2-256"                                | true
      "ssh-ed25519"                                 | true
      "ssh-ed25519-cert-v01@openssh.com"            | false
      "sk-ssh-ed25519@openssh.com"                  | false
      "sk-ssh-ed25519-cert-v01@openssh.com"         | false
      "ssh-rsa"                                     | false
      "ssh-dss"                                     | false
      "ecdsa-sha2-nistp256"                         | false
      "ecdsa-sha2-nistp384"                         | false
      "ecdsa-sha2-nistp521"                         | false
      "sk-ecdsa-sha2-nistp256@openssh.com"          | false
      "ssh-rsa-cert-v01@openssh.com"                | false
      "ssh-dss-cert-v01@openssh.com"                | false
      "ecdsa-sha2-nistp256-cert-v01@openssh.com"    | false
      "ecdsa-sha2-nistp384-cert-v01@openssh.com"    | false
      "ecdsa-sha2-nistp521-cert-v01@openssh.com"    | false
      "sk-ecdsa-sha2-nistp256-cert-v01@openssh.com" | false
    end

    with_them do
      specify { expect(signature_algorithm_allowed?(algorithm)).to eq(expected) }
    end
  end

  describe '#hash_algorithm_allowed?' do
    using RSpec::Parameterized::TableSyntax

    where(:algorithm, :expected) do
      "sha512" | true
      "sha256" | true
      "sha224" | false
      "sha384" | false
      "md5"    | false
    end

    with_them do
      specify { expect(hash_algorithm_allowed?(algorithm)).to eq(expected) }
    end
  end
end
