# frozen_string_literal: true
RSpec.describe SshSig::Blob do
  describe '#from_armor' do
    subject(:from_armor) { described_class.from_armor(armor) }

    context 'when armor was signed by an Ed25519 key' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgtc+Qk8jhMwVZk/jFEFCM16LNQb
        30q5kK30bbetfjyTMAAAAEZmlsZQAAAAAAAAAGc2hhNTEyAAAAUwAAAAtzc2gtZWQyNTUx
        OQAAAECM0+ytbujMHVma7rmy2mF2W3pbn1d2oTmCIzouGOOTtqNaMUB8oAAlkbN9MGa85w
        dzAbFG/SwfjZzzZWffVfcH
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'decodes successfully' do
        expect { from_armor }.not_to raise_error
      end

      it 'has correct attributes' do
        blob = from_armor

        expect(blob.public_key_untrusted).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(blob.public_key_untrusted.verify_key.to_bytes).to eq_hex(
          <<~EOF
          b5 cf 90 93 c8 e1 33 05 59 93 f8 c5 10 50 8c d7
          a2 cd 41 bd f4 ab 99 0a df 46 db 7a d7 e3 c9 33
          EOF
        )
        expect(blob.namespace).to eq("file")
        expect(blob.hash_algorithm).to eq("sha512")
        expect(blob.signature.algorithm).to eq("ssh-ed25519")
        # Ed25519 signatures are always 64 bytes
        expect(blob.signature.bytes.length).to eq(64)
        expect(blob.signature.bytes).to eq_hex(
          <<~EOF
          8c d3 ec ad 6e e8 cc 1d 59 9a ee b9 b2 da 61 76
          5b 7a 5b 9f 57 76 a1 39 82 23 3a 2e 18 e3 93 b6
          a3 5a 31 40 7c a0 00 25 91 b3 7d 30 66 bc e7 07
          73 01 b1 46 fd 2c 1f 8d 9c f3 65 67 df 55 f7 07
          EOF
        )
      end
    end

    context 'when armor was signed by an RSA key' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAAZcAAAAHc3NoLXJzYQAAAAMBAAEAAAGBAL62dt/CGboQhwfa5v9Mra
        1V4OgTu85iWFHVmTbkxQeZVNG/zM9nx+SHOA9pnOA5ZBIbylmYBZpd++31rK4vvT82zor6
        184zlSGW/UryVUp0ADYIH0axHMZwT9pi4jV1i4yr6jM6DWplzg1sttZAmEnIl03ut5ESZP
        3dfHY2xq7Y8I4xD0nasLkSZTP9b54MmIWE9iJSNBt7hhaXgfm9yOPQFVd+Lzty51Rr43Co
        gcNj6+J20u/TZtXbaA9e9IpXGSXDYt+cMoklOQIS0nQzXYPQThjTdgcXK4s2s2PvyhQZF6
        tKvmlNZXBi6suc4lFbluHD9b1+fcAx6ZDcTo0SGIWVJtfum6PWrduR1H/LUi9aqs6n26fP
        O8nYMZQaxrW13q694PGFxQy95v0WihKiGKH28a3wHCAk8u9pZojg6+a6de6wQLVMKbWDmB
        GiZ9rDCbn7CE/vkIKdFx12WqslSdyW7WzMRv8ygAd+hOpiTl589Ka8mlRkHmBBfilqDNiZ
        VwAAAARmaWxlAAAAAAAAAAZzaGE1MTIAAAGUAAAADHJzYS1zaGEyLTUxMgAAAYBRd05XO2
        8lIZg+iDaX85glEksF5+dLRyNMvEVV+HA5Oqa5Mr88MYPf4aUd+nJpilTRsD2sgUnzPYMm
        k2M2TWI2Q+4nFlpQ7eOWrIAOIACC0MxtH2O3/x1WQEZ36lPBXroO8WEs+BDpXTs61hKGQw
        mZuTEUsmblYuyoMEs3NEf1bMoBCgNNgV8NobnC7Vwzs8GJypbMlkf9ntXLzNUhaqzMPUOz
        +aegVKWqSkA1PrpTmaKL48aYJ/JAbcZjt4oFd7MKwg5omeiMyoZ8umUxWpl+0iQo9b7zHi
        C/2c3qxk/i2HZXSy2J+XqsuVjEosKsfFf+qX6HBgRRuv8Ge/1XSZVKRXVBqrFvQ9DnYJ/w
        1zB4jzrjnsbzg1jJTJd2vai2TYUIJyXQBfPfiXRdC8B36Ipu1vsIbf2b1KQX6OXE/zyI5G
        Jpt7OWJEppMZAZaacfAN3cViRtAYLaHAfPzd3EKlj2oSWvLkdmaQkFTa3QWsT/oCE4eqPS
        8MboA//OVT+7JMo=
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'decodes successfully' do
        expect { from_armor }.not_to raise_error
      end

      it 'has correct attributes' do
        blob = from_armor

        expect(blob.public_key_untrusted).to be_a(::OpenSSL::PKey::RSA)
        expect(blob.namespace).to eq("file")
        expect(blob.hash_algorithm).to eq("sha512")
        expect(blob.signature.algorithm).to eq("rsa-sha2-512")

        # Is this signature data correct? Needs to be verified through testing.
        expect(blob.signature.bytes).to eq_hex(
          <<~EOF
          51 77 4e 57 3b 6f 25 21 98 3e 88 36 97 f3 98 25
          12 4b 05 e7 e7 4b 47 23 4c bc 45 55 f8 70 39 3a
          a6 b9 32 bf 3c 31 83 df e1 a5 1d fa 72 69 8a 54
          d1 b0 3d ac 81 49 f3 3d 83 26 93 63 36 4d 62 36
          43 ee 27 16 5a 50 ed e3 96 ac 80 0e 20 00 82 d0
          cc 6d 1f 63 b7 ff 1d 56 40 46 77 ea 53 c1 5e ba
          0e f1 61 2c f8 10 e9 5d 3b 3a d6 12 86 43 09 99
          b9 31 14 b2 66 e5 62 ec a8 30 4b 37 34 47 f5 6c
          ca 01 0a 03 4d 81 5f 0d a1 b9 c2 ed 5c 33 b3 c1
          89 ca 96 cc 96 47 fd 9e d5 cb cc d5 21 6a ac cc
          3d 43 b3 f9 a7 a0 54 a5 aa 4a 40 35 3e ba 53 99
          a2 8b e3 c6 98 27 f2 40 6d c6 63 b7 8a 05 77 b3
          0a c2 0e 68 99 e8 8c ca 86 7c ba 65 31 5a 99 7e
          d2 24 28 f5 be f3 1e 20 bf d9 cd ea c6 4f e2 d8
          76 57 4b 2d 89 f9 7a ac b9 58 c4 a2 c2 ac 7c 57
          fe a9 7e 87 06 04 51 ba ff 06 7b fd 57 49 95 4a
          45 75 41 aa b1 6f 43 d0 e7 60 9f f0 d7 30 78 8f
          3a e3 9e c6 f3 83 58 c9 4c 97 76 bd a8 b6 4d 85
          08 27 25 d0 05 f3 df 89 74 5d 0b c0 77 e8 8a 6e
          d6 fb 08 6d fd 9b d4 a4 17 e8 e5 c4 ff 3c 88 e4
          62 69 b7 b3 96 24 4a 69 31 90 19 69 a7 1f 00 dd
          dc 56 24 6d 01 82 da 1c 07 cf cd dd c4 2a 58 f6
          a1 25 af 2e 47 66 69 09 05 4d ad d0 5a c4 ff a0
          21 38 7a a3 d2 f0 c6 e8 03 ff ce 55 3f bb 24 ca
          EOF
        )
      end
    end

    context 'when header is missing' do
      let(:armor) do
        <<~EOF
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgJKxoLBJBivUPNTUJUSslQTt2hD
        jozKvHarKeN8uYFqgAAAADZm9vAAAAAAAAAFMAAAALc3NoLWVkMjU1MTkAAABAKNC4IEbt
        Tq0Fb56xhtuE1/lK9H9RZJfON4o6hE9R4ZGFX98gy0+fFJ/1d2/RxnZky0Y7GojwrZkrHT
        FgCqVWAQ==
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError)
      end
    end

    context 'when footer is missing' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgJKxoLBJBivUPNTUJUSslQTt2hD
        jozKvHarKeN8uYFqgAAAADZm9vAAAAAAAAAFMAAAALc3NoLWVkMjU1MTkAAABAKNC4IEbt
        Tq0Fb56xhtuE1/lK9H9RZJfON4o6hE9R4ZGFX98gy0+fFJ/1d2/RxnZky0Y7GojwrZkrHT
        FgCqVWAQ==
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError)
      end
    end

    context 'when signature data is missing' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgtc+Qk8jhMwVZk/jFEFCM16LNQb
        30q5kK30bbetfjyTMAAAAEZmlsZQAAAAAAAAAGc2hhNTEy
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError, 'Signature is missing signed data')
      end
    end

    context 'when hash algorithm is missing' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgtc+Qk8jhMwVZk/jFEFCM16LNQb
        30q5kK30bbetfjyTMAAAAEZmlsZQ==
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError, 'Signature is missing hash algorithm')
      end
    end

    context 'when namespace is missing' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgtc+Qk8jhMwVZk/jFEFCM16LNQb
        30q5kK30bbetfjyTM=
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError, 'Signature is missing namespace')
      end
    end

    context 'when public key is missing' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADM=
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError, 'Signature is missing public key')
      end
    end

    context 'when version is unsupported' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAgAAADMAAAALc3NoLWVkMjU1MTkAAAAgtc+Qk8jhMwVZk/jFEFCM16LNQb
        30q5kK30bbetfjyTMAAAAEZmlsZQAAAAAAAAAGc2hhNTEyAAAAUwAAAAtzc2gtZWQyNTUx
        OQAAAECM0+ytbujMHVma7rmy2mF2W3pbn1d2oTmCIzouGOOTtqNaMUB8oAAlkbN9MGa85w
        dzAbFG/SwfjZzzZWffVfcH
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError, 'Unsupported signature version')
      end
    end

    context 'when magic preamble is incorrect' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lIAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgtc+Qk8jhMwVZk/jFEFCM16LNQb
        30q5kK30bbetfjyTMAAAAEZmlsZQAAAAAAAAAGc2hhNTEyAAAAUwAAAAtzc2gtZWQyNTUx
        OQAAAECM0+ytbujMHVma7rmy2mF2W3pbn1d2oTmCIzouGOOTtqNaMUB8oAAlkbN9MGa85w
        dzAbFG/SwfjZzzZWffVfcH
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError, 'Invalid magic preamble')
      end
    end

    context 'when non-base64 characters are present' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgJKxoLBJBivUPNTUJUSslQTt2hD
        jozKvHarKeN8uYFqgAAAADZm9vAAAAAAAAAFMAAAALc3NoLWVkMjU1MTkAAABAKNC4IEbt
        Tq0Fb56xhtuE1/lK9H9RZJfON4o6hE9R4ZGFX98gy0+fFJ/1d2/RxnZky0Y7GojwrZkrHT
        FgC!!!!!!!!!!!qVWAQ==
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError)
      end
    end

    context 'when base64 is not correctly padded' do
      let(:armor) do
        <<~EOF
        -----BEGIN SSH SIGNATURE-----
        U1NIU0lHAAAAAQAAADMAAAALc3NoLWVkMjU1MTkAAAAgJKxoLBJBivUPNTUJUSslQTt2hD
        jozKvHarKeN8uYFqgAAAADZm9vAAAAAAAAAFMAAAALc3NoLWVkMjU1MTkAAABAKNC4IEbt
        Tq0Fb56xhtuE1/lK9H9RZJfON4o6hE9R4ZGFX98gy0+fFJ/1d2/RxnZky0Y7GojwrZkrHT
        FgCqVWAQ
        -----END SSH SIGNATURE-----
        EOF
      end

      it 'raises error' do
        expect { from_armor }.to raise_error(SshSig::DecodeError)
      end
    end
  end

  describe '#signature_data' do
    let(:namespace) { "file" }
    let(:hash_algorithm) { "sha512" }
    let(:message) { "This is a test of SshSig::Blob#signature_data" }

    subject(:signature_data) do
      described_class.new(
        namespace: namespace,
        hash_algorithm: hash_algorithm,
        public_key: nil,
        signature: nil
      ).signature_data(message)
    end

    context 'when hash_algorithm is sha512' do
      it 'returns expected data' do
        preamble = "SSHSIG".b
        namespace = "\x00\x00\x00\x04file".b
        reserved = "\x00\x00\x00\x00".b
        hash_algorithm = "\x00\x00\x00\x06sha512".b
        digest = hex_to_bin(
          <<~EOF
          00 00 00 40 e7 c1 3e 82 31 b1 15 8e 49 29 98 4f
          2e 58 b7 aa 65 3d 47 1a 40 99 b1 d0 b6 4d ec 2c
          31 c2 90 2e 6a 26 63 57 6b f4 0e 4b 5b 6b 97 93
          92 44 b5 53 de 36 ac bb 78 3c 42 54 cc 70 f5 a7
          22 08 70 1b
          EOF
        )

        expect(signature_data).to eq(preamble + namespace + reserved + hash_algorithm + digest)
      end
    end

    context 'when hash_algorithm is sha256' do
      let(:hash_algorithm) { "sha256" }

      it 'returns expected data' do
        preamble = "SSHSIG".b
        namespace = "\x00\x00\x00\x04file".b
        reserved = "\x00\x00\x00\x00".b
        hash_algorithm = "\x00\x00\x00\x06sha256".b
        digest = hex_to_bin(
          <<~EOF
          00 00 00 20 b0 13 c3 6c 6b 27 17 df bb 9f 05 64
          49 30 b2 84 24 9d 63 30 a2 77 1a 7f a6 4c 0f 37
          d7 f3 5c f9
          EOF
        )

        expect(signature_data).to eq(preamble + namespace + reserved + hash_algorithm + digest)
      end
    end
  end
end
