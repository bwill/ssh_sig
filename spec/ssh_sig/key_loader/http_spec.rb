# frozen_string_literal: true
RSpec.describe SshSig::KeyLoader::Http do
  describe '#load' do
    let(:uri) { "https://example.com/example.keys" }

    subject(:load) { described_class.load(uri) }

    before do
      allow(described_class).to receive(:get).with(uri).and_return(keys)
    end

    context 'with single key' do
      let(:keys) { "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN4Qo3O/qt/piAelNKQnbeu3CW2R7UPW8B1N/FhB8jPl comment" }

      it 'loads successfully' do
        pubkey = load.first

        expect(pubkey).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(pubkey.verify_key.to_bytes).to eq_hex(
          <<~EOF
          de 10 a3 73 bf aa df e9 88 07 a5 34 a4 27 6d eb
          b7 09 6d 91 ed 43 d6 f0 1d 4d fc 58 41 f2 33 e5
          EOF
        )
      end
    end

    context 'with multiple keys' do
      let(:keys) do
        <<~EOF
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILXPkJPI4TMFWZP4xRBQjNeizUG99KuZCt9G23rX48kz
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPVB9SHfnHpZJY4HiQQ9K5Pa7cTuiyCldEttd2JuGdSS
        EOF
      end

      it 'loads each of them' do
        first, second = load

        expect(first).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(first.verify_key.to_bytes).to eq_hex(
          <<~EOF
          b5 cf 90 93 c8 e1 33 05 59 93 f8 c5 10 50 8c d7
          a2 cd 41 bd f4 ab 99 0a df 46 db 7a d7 e3 c9 33
          EOF
        )

        expect(second).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(second.verify_key.to_bytes).to eq_hex(
          <<~EOF
          f5 41 f5 21 df 9c 7a 59 25 8e 07 89 04 3d 2b 93
          da ed c4 ee 8b 20 a5 74 4b 6d 77 62 6e 19 d4 92
          EOF
        )
      end
    end
  end

  describe '#load_dot_keys' do
    let(:keys) do
      <<~EOF
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILXPkJPI4TMFWZP4xRBQjNeizUG99KuZCt9G23rX48kz
      ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPVB9SHfnHpZJY4HiQQ9K5Pa7cTuiyCldEttd2JuGdSS
      EOF
    end

    it 'calls #get with correct URL' do
      expect(described_class).to receive(:get).with('https://gitlab.com/user.keys').and_return(keys)
      described_class.load_dot_keys('user')

      expect(described_class).to receive(:get).with('https://example.com/user.keys').and_return(keys)
      described_class.load_dot_keys('user', 'https://example.com')
    end
  end
end
