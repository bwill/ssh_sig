# frozen_string_literal: true
RSpec.describe SshSig::KeyLoader::PubKey do
  describe '#load' do
    subject(:load) { described_class.load(armored_pubkey).first }

    context 'with ed25519 key' do
      let(:armored_pubkey) do
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN4Qo3O/qt/piAelNKQnbeu3CW2R7UPW8B1N/FhB8jPl comment"
      end

      it 'loads successfully' do
        pubkey = load

        expect(pubkey).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(pubkey.verify_key.to_bytes).to eq_hex(
          <<~EOF
          de 10 a3 73 bf aa df e9 88 07 a5 34 a4 27 6d eb
          b7 09 6d 91 ed 43 d6 f0 1d 4d fc 58 41 f2 33 e5
          EOF
        )
      end

      context 'when comment is missing' do
        let(:armored_pubkey) do
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN4Qo3O/qt/piAelNKQnbeu3CW2R7UPW8B1N/FhB8jPl"
        end

        it 'loads successfully' do
          pubkey = load

          expect(pubkey).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
          expect(pubkey.verify_key.to_bytes).to eq_hex(
            <<~EOF
            de 10 a3 73 bf aa df e9 88 07 a5 34 a4 27 6d eb
            b7 09 6d 91 ed 43 d6 f0 1d 4d fc 58 41 f2 33 e5
            EOF
          )
        end
      end

      context 'with truncated key' do
        let(:armored_pubkey) { "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN4Qo3O" }

        it 'raises error' do
          expect { load }.to raise_error(::SshSig::LoadError)
        end
      end
    end

    context 'with RSA key' do
      let(:armored_pubkey) do
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDMkeIxxTVriDeYxnioEZfIacf3hRl6y" \
        "/o8OUjkvn5YHKGduR9d+Z10HUQlI3KRDSbNfymO9LfUd6IzNBFD9h3FFE3TEguwrZbaRE" \
        "nGZ35bOgoqKUM5uanO2O8hSC3YBPRTkTFcDG36H5hVp7Z+x8hSoixIwz2QaEfK8n6e17G" \
        "18hYlICMKrEFhSrP7sIjpefo2gJqkZCJPdEoRNEwmg5ndah2iK5hZuUJ8PixamOW4JJ8D" \
        "zm5eVYvvHuXENeXpQ3Bbx6K/lDlOUQJlC+InixkPvYiXxik3TDQCkaZmDWZpSceQNUcTf" \
        "t2xTl6mtxQuvplQ39rMGX0s8FTdmLwv6yLRpZDp/kYhr6U2C6er7mcW0Bkor0xyOTYsVP" \
        "BG5xxD1iAEtjuDNZmZk1vd0LC44cw8jIAaQgGZDR/lP9nzYKmj2FsG17pRxotp2lhOmig" \
        "SXozAHZ6d56aBtvy2pGWRt/yWB+7aVN3h6FVVvCaNrDkVd26WADHtrGFhgdphL8z9mws="
      end

      it 'loads successfully' do
        pubkey = load

        expect(pubkey).to be_a(::OpenSSL::PKey::RSA)
        expect(pubkey.n.to_ssh).to eq_hex(
          <<~EOF
          00 00 01 81 00 cc 91 e2 31 c5 35 6b 88 37 98 c6
          78 a8 11 97 c8 69 c7 f7 85 19 7a cb fa 3c 39 48
          e4 be 7e 58 1c a1 9d b9 1f 5d f9 9d 74 1d 44 25
          23 72 91 0d 26 cd 7f 29 8e f4 b7 d4 77 a2 33 34
          11 43 f6 1d c5 14 4d d3 12 0b b0 ad 96 da 44 49
          c6 67 7e 5b 3a 0a 2a 29 43 39 b9 a9 ce d8 ef 21
          48 2d d8 04 f4 53 91 31 5c 0c 6d fa 1f 98 55 a7
          b6 7e c7 c8 52 a2 2c 48 c3 3d 90 68 47 ca f2 7e
          9e d7 b1 b5 f2 16 25 20 23 0a ac 41 61 4a b3 fb
          b0 88 e9 79 fa 36 80 9a a4 64 22 4f 74 4a 11 34
          4c 26 83 99 dd 6a 1d a2 2b 98 59 b9 42 7c 3e 2c
          5a 98 e5 b8 24 9f 03 ce 6e 5e 55 8b ef 1e e5 c4
          35 e5 e9 43 70 5b c7 a2 bf 94 39 4e 51 02 65 0b
          e2 27 8b 19 0f bd 88 97 c6 29 37 4c 34 02 91 a6
          66 0d 66 69 49 c7 90 35 47 13 7e dd b1 4e 5e a6
          b7 14 2e be 99 50 df da cc 19 7d 2c f0 54 dd 98
          bc 2f eb 22 d1 a5 90 e9 fe 46 21 af a5 36 0b a7
          ab ee 67 16 d0 19 28 af 4c 72 39 36 2c 54 f0 46
          e7 1c 43 d6 20 04 b6 3b 83 35 99 99 93 5b dd d0
          b0 b8 e1 cc 3c 8c 80 1a 42 01 99 0d 1f e5 3f d9
          f3 60 a9 a3 d8 5b 06 d7 ba 51 c6 8b 69 da 58 4e
          9a 28 12 5e 8c c0 1d 9e 9d e7 a6 81 b6 fc b6 a4
          65 91 b7 fc 96 07 ee da 54 dd e1 e8 55 55 bc 26
          8d ac 39 15 77 6e 96 00 31 ed ac 61 61 81 da 61
          2f cc fd 9b 0b
          EOF
        )
        expect(pubkey.e.to_ssh).to eq_hex("00 00 00 03 01 00 01")
      end
    end

    context 'with multiple keys' do
      let(:keys) do
        <<~EOF
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILXPkJPI4TMFWZP4xRBQjNeizUG99KuZCt9G23rX48kz
        ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPVB9SHfnHpZJY4HiQQ9K5Pa7cTuiyCldEttd2JuGdSS
        EOF
      end

      it 'loads each of them' do
        first, second = described_class.load(keys)

        expect(first).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(first.verify_key.to_bytes).to eq_hex(
          <<~EOF
          b5 cf 90 93 c8 e1 33 05 59 93 f8 c5 10 50 8c d7
          a2 cd 41 bd f4 ab 99 0a df 46 db 7a d7 e3 c9 33
          EOF
        )

        expect(second).to be_a(::Net::SSH::Authentication::ED25519::PubKey)
        expect(second.verify_key.to_bytes).to eq_hex(
          <<~EOF
          f5 41 f5 21 df 9c 7a 59 25 8e 07 89 04 3d 2b 93
          da ed c4 ee 8b 20 a5 74 4b 6d 77 62 6e 19 d4 92
          EOF
        )
      end
    end
  end
end
