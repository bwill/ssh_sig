# frozen_string_literal: true
RSpec.describe 'openssh' do
  before do
    ssh_keygen_setup!
  end

  let(:fixtures) { ssh_fixtures_for(algorithm) }

  context 'when using ed25519 keys', integration: :openssh do
    let(:algorithm) { "ed25519" }

    it 'verifies signatures created by ssh-keygen' do
      blob = SshSig::Blob.from_armor(fixtures[:signature])
      subject = SshSig::Verifier.from_armored_pubkey(fixtures[:public_key])
        .verify(blob, fixtures[:message])

      expect(subject).to be true
    end
  end

  context 'when using RSA keys', integration: :openssh do
    let(:algorithm) { "rsa" }

    it 'verifies signatures created by ssh-keygen' do
      blob = SshSig::Blob.from_armor(fixtures[:signature])
      subject = SshSig::Verifier.from_armored_pubkey(fixtures[:public_key])
        .verify(blob, fixtures[:message])

      expect(subject).to be true
    end
  end
end
