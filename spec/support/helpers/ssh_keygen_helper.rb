# frozen_string_literal: true
module SshKeygenHelper
  TEST_DIR = 'tmp/test/'
  TEST_MESSAGE = 'This is an integration test for ssh_sig'

  def ssh_keygen_setup!
    setup_test_dir!
  end

  def ssh_fixtures_for(algorithm, message = TEST_MESSAGE)
    keys = generate_keys!(algorithm)
    signature_data = generate_signature!(keys, message)
    keys.merge(signature_data)
  end

  private

  def setup_test_dir!
    FileUtils.rm_rf(TEST_DIR) if File.exist?(TEST_DIR)
    FileUtils.mkdir_p(TEST_DIR)
  end

  def generate_keys!(algorithm)
    private_key_path = File.join(TEST_DIR, "id_#{algorithm}")
    public_key_path = "#{private_key_path}.pub"
    `ssh-keygen -t '#{algorithm}' -f '#{private_key_path}' -N ''`

    {
      public_key: File.read(public_key_path),
      public_key_path: public_key_path,
      private_key: File.read(private_key_path),
      private_key_path: private_key_path
    }
  end

  def generate_signature!(keys, message)
    message_path = File.join(TEST_DIR, 'message.txt')
    signature_path = "#{message_path}.sig"
    File.write(message_path, message)
    `ssh-keygen -Y sign -n file -f '#{keys[:public_key_path]}' '#{message_path}'`

    {
      message: message,
      message_path: message_path,
      signature: File.read(signature_path),
      signature_path: signature_path
    }
  end
end
