# frozen_string_literal: true
module ByteHelpers
  def bin_to_hex(bin)
    hex_string = bin.unpack1("H*")
    bytes = hex_string.chars.each_slice(2).map(&:join)

    # Each line will have 16 bytes
    bytes.each_slice(16).map { |group| group.join(" ") }.join("\n")
  end

  def hex_to_bin(hex)
    [hex.gsub(/\s+/, '')].pack('H*')
  end
end
