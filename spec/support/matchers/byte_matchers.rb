# frozen_string_literal: true

require_relative '../helpers/byte_helpers'

RSpec::Matchers.define :eq_hex do |expected|
  include ByteHelpers

  match do |actual|
    # Format actual into hex so that diffs are in hex and easy to read.
    @actual = bin_to_hex(actual)

    # For actual comparison, decode hex and compare bytes
    expected = hex_to_bin(expected)
    values_match? expected, actual
  end

  diffable
end
